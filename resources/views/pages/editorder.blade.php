@extends('layouts.master')

@section('title','Nalozi')    
@section('meta')
@stop
@section('nalozi','is-active')
@section('navbar_title', 'Nalozi')
@section('head')
@stop

<style type="text/css">
  .c-navbar__title{
    font-weight: normal;
  }
  body{
    font-family: Lato,sans-serif !important;
  }
</style>

@section('content')


<style type="text/css">
  .c-table__cell{
    text-align: center;
  }
</style>


<form action="{{'/nalozi/update/'.$ord->id}}" method="POST">
  {{csrf_field()}}
<div class="container">
    <datalist id="produkti">
        @foreach ($products as $p )
            <option data-id="{{$p->id}}" value="{{$p->id .'-'. $p->product_name}}">{{$p->product_name}}</option>
        @endforeach
       </datalist>
       
       <datalist id="materijali">
          @foreach ($materials as $m )
            <option data-id="{{$m->id}}" value="{{$m->id . '-' . $m->material_name}}">{{$m->material_name}}</option>
          @endforeach
        </datalist>
      <div class="row">
        <div class="col-12">
          <div class="row">
            <div class="col-6">
              <label for="ordernumber">Broj naloga</label>
              <input class="c-input" type="text" value="{{$ord->order_number}}" name="ordernumber" id="ordernumber" placeholder="#" disabled>
              <input type="hidden" value="{{count($orders)}}" name="count" id="count" required>
            </div>
            <div class="col-6">
                <label for="date">Datum doradnje</label>
                <input class="c-input" type="text" name="date" id="date" placeholder="datum" value="{{date('Y-m-d H:i:s')}}" disabled>
              </div>
          </div>
          <br>
          <button type="button" class="c-btn c-btn--success u-mb-xsmall" id="novaStavka"><i class="feather icon-file-plus" style="padding-right: 10px; color: white"></i>Dodaj stavku</button>
          <button type="submit" class="c-btn c-btn--success u-mb-xsmall"><i class="feather icon-check" style="padding-right: 10px; color: white"></i>Pohrani nalog</button>

          <div class="c-table-responsive@wide">
                <table class="c-table" align="center">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head"><strong>Proizvod</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Materijal</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Utrošeno</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Cijena</strong></th>
                      <th class="c-table__cell c-table__cell--head">Ukloni</th>
                    </tr>
                  </thead>

                  <tbody id="body">
                      <?php $cnt = 1;?>

                      @foreach ($orders as $o)
                      <tr id="{{'row'.$cnt}}" class="c-table__row">
                        <td class="c-table__cell"><input id="{{'pinput'.$cnt}}" list="produkti" value="" class="c-input" type="text" name="{{'product'.$cnt}}"  autocomplete=off></td>
                        <td class="c-table__cell"><input id="{{'minput'.$cnt}}" list="materijali" value="" class="c-input" type="text" name="{{'material'.$cnt}}"  autocomplete=off></td>
                        <td class="c-table__cell"><input class="c-input" type="text" value="{{$o->quantity}}" name="{{'quantity'.$cnt}}"  autocomplete=off></td>
                        <td class="c-table__cell"><input class="c-input" type="text" value="{{$o->price}}" name="{{'price'.$cnt}}"  autocomplete=off></td>
                        <input type="hidden" value="o" name="{{'record'.$cnt}}" autocomplete=off>
                        <input type="hidden" value="{{$o->id}}" name="{{'id'.$cnt}}" autocomplete=off>
                          <td class="c-table__cell">
                            <div class="c-dropdown dropdown">
                              <a data-rowid="{{$cnt}}" class="deleteBtn">
                                <i class="feather icon-trash-2" style="padding-left: 10px; color: #2083fe;font-size:25px; cursor: pointer;"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                        <script>
                          $('#produkti').children().each((i,e)=>{
                            if(e.getAttribute('data-id')=="{{$o->product_id}}"){
                              document.querySelector("{{'#pinput'.$cnt}}").value = e.value ;
                            }
                          })

                          $('#materijali').children().each((i,e)=>{
                            if(e.getAttribute('data-id')=="{{$o->material_id}}"){
                              document.querySelector("{{'#minput'.$cnt}}").value = e.value ;
                            }
                          })
                        </script>
                    <?php $cnt++;?>

                        @endforeach
                  </tbody>

                  </table>
                </div>
              </div>
            </div>
     </div>
    </form>
      @stop

      @section('script')
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
    <script>
      let cnt = {{$cnt}};
    
      
      $('#novaStavka').click(()=>{
        cnt++;
        $('#count').val(cnt);
      $('#body').append('<tr id="row'+cnt+'" class="c-table__row">'+
                      '<td class="c-table__cell"><input list="produkti" class="c-input" type="text" name="product'+ cnt +'"  autocomplete=off></td>'+
                      '<td class="c-table__cell"><input list="materijali" class="c-input" type="text" name="material'+ cnt +'"  autocomplete=off></td>'+
                      '<td class="c-table__cell"><input class="c-input" type="text" name="quantity'+ cnt +'"  autocomplete=off></td>'+
                      '<td class="c-table__cell"><input class="c-input" type="text" name="price'+ cnt +'" autocomplete=off ></td>'+
                      '<input type="hidden" value="n" name="record'+cnt+'" autocomplete=off>'+
                      '<td class="c-table__cell">'+
                          '<div class="c-dropdown dropdown">'+
                          '<a data-rowid="'+cnt+'" class="deleteBtn">'+
                            '<i class="feather icon-trash-2" style="padding-left: 10px; color: #2083fe;font-size:25px; cursor: pointer;"></i>'+
                          '</a>'+
                          '</div>'+
                      '</td>'+
                      '</tr>');
      });

      let body = '#body';
      $('#body').on('click','a',(e)=>{
        e.preventDefault();
        let id = $(e.currentTarget).data('rowid');
        let row = body+' #row'+id;
        $(row).css('display','none');
        $(row).css('visibility','hidden');
        if($(row+' input[name="record'+id+'"]').val()=='o')
          $(row+' input[name="record'+id+'"]').val('od')
        else
          $(row+' input[name="record'+id+'"]').val('d');
      });

      
    </script>
      @stop