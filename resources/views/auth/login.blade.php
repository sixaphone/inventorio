@extends('layouts.app')

@section('title','Prijava')

@section('content')
    <div class="o-page o-page--center">
      <div class="o-page__card">
        <div class="c-card c-card--center">
          <form method="POST" action="{{ route('login') }}">
          {{csrf_field()}}

            <div class="c-field">
              <label class="c-field__label">Email</label>
              <input id="email" name="email" value="{{ old('email') }}" class="c-input u-mb-small" type="email" placeholder="Unesite Vaš email" required>
            </div>
            
            <div class="c-field">
              <label class="c-field__label">Lozinka</label>
              <input class="c-input u-mb-small" value="{{ old('password') }}" id="password" name="password" type="password" placeholder="Unesite Vašu lozinku" required>
              @if ($errors->has('email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  <br>
              @endif
            </div>
            <button type="submit" class="c-btn c-btn--fullwidth c-btn--info">Prijava</button>
          </form>
        </div>
      </div>
    </div>
@stop

@section('script')
@stop
    