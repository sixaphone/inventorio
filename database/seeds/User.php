<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => 'zijad',
            'email' => 'zijad@gmail.com',
            'password' => bcrypt('tishleri'),
        ]);
    }
}
