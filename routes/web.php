<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {


    //CRUD PRODUCT
    Route::get('/produkti', 'ProductController@index');
    Route::get('/produkti/{id}', 'ProductController@show');
    Route::post('/produkti/new', 'ProductController@store');
    Route::post('/produkti/update', 'ProductController@update');
    //CRUD PRODUCT
    
    //CRUD MATERIAL
    Route::get('/materijali', 'MaterialController@index');
    Route::get('/materijali/{id}', 'MaterialController@show');
    Route::post('/materijali/new', 'MaterialController@store');
    Route::post('/materijali/update', 'MaterialController@update');
    //CRUD MATERIAL
    
    
    //CRUD ORDER
    Route::get('/nalozi', 'OrderController@index');
    Route::get('/', 'OrderController@index');
    Route::get('/home', 'OrderController@index')->name('home');
    Route::get('/nalozi/show/{id}', 'OrderController@show');
    Route::get('/nalozi/new', 'OrderController@create');
    Route::get('/nalozi/update/{id}', 'OrderController@edit');
    Route::post('/nalozi/new', 'OrderController@store');
    Route::post('/nalozi/update/{id}', 'OrderController@update');
    //CRUD ORDER
    
    
    
    
});

Auth::routes();