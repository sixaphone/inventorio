@extends('layouts.master')

@section('title','Nalozi')    
@section('meta')
@stop
@section('nalozi','is-active')
@section('navbar_title', 'Nalozi')
@section('head')
@stop

<style type="text/css">
  .c-navbar__title{
    font-weight: normal;
  }
  body{
    font-family: Lato,sans-serif !important;
  }
</style>

@section('content')


<style type="text/css">
  .c-table__cell{
    text-align: center;
  }
</style>


<form action="/nalozi/new" method="POST">
  {{csrf_field()}}
<div class="container">
      <div class="row">
        <div class="col-12">
          <div class="row">
            <div class="col-6">
              <label for="ordernumber">Broj naloga</label>
              <input class="c-input" type="text" name="ordernumber" id="ordernumber" placeholder="#" required>
              <input type="hidden" name="count" id="count" required>
            </div>
            <div class="col-6">
                <label for="date">Datum kreiranja</label>
                <input class="c-input" type="text" name="date" id="date" placeholder="datum" value="{{date('Y-m-d H:i:s')}}" required>
              </div>
          </div>
          <br>
          <button type="button" class="c-btn c-btn--success u-mb-xsmall" id="novaStavka"><i class="feather icon-file-plus" style="padding-right: 10px; color: white"></i>Dodaj stavku</button>
          <button type="submit" class="c-btn c-btn--success u-mb-xsmall"><i class="feather icon-check" style="padding-right: 10px; color: white"></i>Pohrani nalog</button>

          <div class="c-table-responsive@wide">
                <table class="c-table" align="center">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head"><strong>Proizvod</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Materijal</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Utrošeno</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Cijena</strong></th>
                      <th class="c-table__cell c-table__cell--head">Ukloni</th>
                    </tr>
                  </thead>
                  <tbody id="body">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
     </div>
     <datalist id="produkti">
      @foreach ($products as $p )
          <option value="{{$p->id .'-'. $p->product_name}}">{{$p->product_name}}</option>
      @endforeach
     </datalist>
     
     <datalist id="materijali">
        @foreach ($materials as $m )
          <option value="{{$m->id . '-' . $m->material_name}}">{{$m->material_name}}</option>
        @endforeach
      </datalist>
    </form>
      @stop

      @section('script')
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
    <script>
      let cnt = 0;
      $('.printBtn').click((e)=>{
        e.preventDefault();
          $('#modalPrisustvo').modal('show');
        
      });
      
      $('#novaStavka').click(()=>{
        cnt++;
        $('#count').val(cnt);
      $('#body').append('<tr id="row'+cnt+'" class="c-table__row">'+
                      '<td class="c-table__cell"><input list="produkti" class="c-input" type="text" name="product'+ cnt +'"  required></td>'+
                      '<td class="c-table__cell"><input list="materijali" class="c-input" type="text" name="material'+ cnt +'"  required></td>'+
                      '<td class="c-table__cell"><input class="c-input" type="text" name="quantity'+ cnt +'"  required></td>'+
                      '<td class="c-table__cell"><input class="c-input" type="text" name="price'+ cnt +'"  required></td>'+
                      '<td class="c-table__cell">'+
                          '<div class="c-dropdown dropdown">'+
                          '<a data-rowid="'+cnt+'" class="deleteBtn">'+
                            '<i class="feather icon-trash-2" style="padding-left: 10px; color: #2083fe;font-size:25px; cursor: pointer;"></i>'+
                          '</a>'+
                          '</div>'+
                      '</td>'+
                      '</tr>');
      });

      let body = '#body';
      $('#body').on('click','a',(e)=>{
        e.preventDefault();
        let id = $(e.currentTarget).data('rowid');
        let row = body+' #row'+id;
        $(row).remove();
      });

      
    </script>
      @stop