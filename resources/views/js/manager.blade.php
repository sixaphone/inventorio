<script>
  let months = ['Januar','Februar','Mart','April','Maj','Juni','Juli','August','Septembar','Oktobar','Novembar','Decembar']
  let monthPicked = yearPicked = 0;
  /* STATS*/
  // $.get("{{url(session('company_id') . '/worker/crates/' . $worker->id)}}",function(res){
    //     document.querySelector('#c_today').innerHTML = res.today;
    //     document.querySelector('#c_week').innerHTML = res.this_week;
    //     document.querySelector('#c_month').innerHTML = res.this_month;
    //     document.querySelector('#c_year').innerHTML = res.this_year;
    //     if(res.today){
      //       if(res.today < res.yesterday){
        //         $('#icon-c-today').addClass('icon-trending-down')
        //         $('#icon-c-today').addClass('u-color-danger')
        //       }
        //       else{
          //         $('#icon-c-today').addClass('icon-trending-up')
          //         $('#icon-c-today').addClass('u-color-success')
          //       }
          //     }
          
          //     if(res.this_week){
            //       if(res.this_week < res.last_week){
              //         $('#icon-c-week').addClass('icon-trending-down')
              //         $('#icon-c-week').addClass('u-color-danger')
              //       }
              //       else{
                //         $('#icon-c-week').addClass('icon-trending-up')
                //         $('#icon-c-week').addClass('u-color-success')
                //       }
                //     }
                
                //     if(res.this_month){
                  //       if(res.this_month < res.last_month){
                    //         $('#icon-c-month').addClass('icon-trending-down')
                    //         $('#icon-c-month').addClass('u-color-danger')
                    //       }
                    //       else{
                      //         $('#icon-c-month').addClass('icon-trending-up')
                      //         $('#icon-c-month').addClass('u-color-success')
                      //       }
                      //     }
                      //     if(res.this_year){
                        //       if(res.this_year < res.last_year){
                          //         $('#icon-c-year').addClass('icon-trending-down')
                          //         $('#icon-c-year').addClass('u-color-danger')
                          //       }
                          //       else{
                            //         $('#icon-c-year').addClass('icon-trending-up')
                            //         $('#icon-c-year').addClass('u-color-success')
                            //       }
                            //     }          
                            
                            //   });
                            /* STATS*/
                            
                            
                            
                            /*GRAPHS*/
                            /*YEAR*/
$('.year-picker').click(function(){
    document.querySelector('#year').value = $(this).data('year');
    yearPicked = $(this).data('year');
                     $.ajax({
    url:"{{url(session('company_id') . '/api/manager/graph/year').'/'}}" + document.querySelector('#year').value + '/' + document.querySelector('#worker_id').value,
    type:'get',
    error:(err)=>{console.log(err);}
  })           
    setTimeout(function(){
      $.get("{{url(session('company_id') . '/api/manager/graph/year').'/'}}" + document.querySelector('#year').value + '/' + document.querySelector('#worker_id').value,function(res){
        let p = document.querySelector('#GodinaTekst');
        p.innerHTML = "Radnik je u ovoj godini skenirao ukupno  <strong>" + res.ServerResponse.Sum + "</strong> gajbi.";
        p.innerHTML += " Ukupno je radio <strong>" + res.ServerResponse.Days + " dana</strong>";
        p.innerHTML += " ili <strong> "+ res.ServerResponse.Hours +" sata/i</strong>";
        p.innerHTML += ". Nisu zabilježene nepravilnosti niti napomene.";
        let highest = Math.max(...res.ServerResponse.Collected);
        new Chartist.Bar('.two-lines-chartc', {
          labels: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
          series: [
            res.ServerResponse.Collected
          ]
        }, {
          fullWidth: true,
          height: '300px',
          width: '500px',
          chartPadding: {
            right: 0,
            bottom: 0,
            left: 0,
            right: 20
          },
          axisX: {
            showLabel: true,
            showGrid: true
          },
          axisY: {
            showLabel: true,
            showGrid: true
          },
          high : highest+8
          
        });
      });
    },500);
        
  });
  /*YEAR*/

/*MONTH*/
$('.month-picker').click(function(){
  document.querySelector('#month').value = $(this).data('month');
  monthPicked = $(this).data('month');
  setTimeout(function(){
    $.get("{{url(session('company_id') . '/api/manager/graph/month').'/'}}"+document.querySelector('#month').value + '/' + document.querySelector('#worker_id').value,function(res){
      let p = document.querySelector('#MjesecTekst');
        p.innerHTML = "Radnik je u ovom mjesecu skenirao ukupno <strong>" + res.ServerResponse.Sum + "</strong> gajbi.";
        p.innerHTML += " Ukupno je radio <strong>" + res.ServerResponse.Days + " dana</strong>";
        p.innerHTML += " ili <strong> "+ res.ServerResponse.Hours +" sati</strong>";
        p.innerHTML += ". Nisu zabilježene nepravilnosti niti napomene.";
        let highest = Math.max(...res.ServerResponse.Collected);
        new Chartist.Bar('.two-lines-chartb', {
          labels: res.ServerResponse.DaysLabel,
          series: 
          [res.ServerResponse.Collected]
          
        }, {
          fullWidth: true,
          height: '300px',
          width: '600px',
          chartPadding: {
            right: 0,
            bottom: 0,
            left: 0,
            right: 20
          },
          axisX: {
            showLabel: true,
            showGrid: true
          },
          axisY: {
            showLabel: true,
            showGrid: true
          },
          high : highest+8
          
        });
      });
    },300);
  });
  /*MONTH*/
  
  /*DAY*/
  $('#day-picker').click(function(){
    $.get("{{url(session('company_id') . '/api/manager/graph/hour') .'/'}}"+document.querySelector('#worker_id').value,function(res){
      let p = document.querySelector('#DnevnaTekst'); 
      p.innerHTML = "Radnik je danas skenirao ukupno <strong>" + res.ServerResponse.Sum +"</strong> gajbi." 
      if(res.ServerResponse.first)
      p.innerHTML += "aktivnost prijavljena je u <strong>" + res.ServerResponse.first + "</strong>" 
      if(res.ServerResponse.last)
      p.innerHTML += ",a zadnja u <strong>" + res.ServerResponse.last + "</strong>"
      p.innerHTML += ". Nisu zabilježene nepravilnosti niti napomene.</p>";
      let highest = Math.max(...res.ServerResponse.Collected);
      setTimeout(function(){
      new Chartist.Line('.two-lines-charta', {
        labels: res.ServerResponse.Hours,
        series: [
          res.ServerResponse.Collected
        ]
      }, {
        fullWidth: true,
          height: '300px',
          width: '500px',
          chartPadding: {
            right: 30,
            bottom: 0,
            left: 0
          },
          axisX: {
            showLabel: true,
            showGrid: true,
          },
          axisY: {
            showLabel: true,
            showGrid: true,
          },
          high : highest+8
        });
      },300);
    });
  });
  /*DAY*/  
/*GRAPHS*/


/*PDF*/
/*DAY*/

function GenerateHTMLForDay(res,multiple=0){
  let prviPrint = "<strong>Ukupno skenirao gajbi: </strong>" + res.count + "<br>";
  let prvaAktivnost = zadnjaAktivnost = 'Nezabilježena';
  if(res.count > 0){
    prvaAktivnost = res.barcodes[0].timestamp.split(" ")[1];
    zadnjaAktivnost = res.barcodes[res.count-1].timestamp.split(" ")[1];
  }
  
  prviPrint += "<strong>Prva aktivnost: </strong>" + prvaAktivnost + "<br>";
  prviPrint += "<strong>Zadnja aktivnost: </strong>" + zadnjaAktivnost + "<br>";
  prviPrint += "<strong>Ukupno sati: </strong>" + res.hours + "<br><br>";
  prviPrint += "<strong>Detaljan uvid u transakcije</strong><br><br>";
  if(multiple)prviPrint += '<div class="tabAl">';
      
    prviPrint +="<table border='1'>"+
      "<tr>"+
        "<th>ID zapisa</th>"+
        "<th>Vrijeme unosa</th>"+
        "<th>Berač</th>"+
        "<th>Količina</th>"+
        "<th>Ukupno</th>"+
        "</tr>";
        for(let i = 0; i < res.count;i++){
          prviPrint += "<tr>"
            + "<td>" + Math.random().toString(36).slice(-5) + "</td>"
            + "<td>" + res.barcodes[i].timestamp + "</td>"
            + "<td>" + res.barcodes[i].name + ' ' + res.barcodes[i].surname + "</td>"
            + "<td>" + 1 + "</td>"
            + "<td>" + (i+1) + "</td>"
            + "</tr>";
          }
          if(multiple)prviPrint += "</div>";
          
          return prviPrint;
        } 
        
$('#prviPrintBtn').click(()=>{
  $.get("{{url(session('company_id') . '/api/manager/PDF/day').'/'}}" + document.querySelector('#worker_id').value + '/' + today,(res)=>{
    document.querySelector('#prviPrintTabela').innerHTML = GenerateHTMLForDay(res);  
    printJS('prviPrint', 'html');
  });
});
        /*DAY*/

/*MONTH BASIC*/
$('#treciPrintBtn').click(()=>{
  let date = yyyy+'-' + monthPicked.toString().padStart(2,'0') +'-01';
  $.get("{{url(session('company_id') . '/api/manager/PDF/month').'/'}}" + document.querySelector('#worker_id').value + '/' + date,(res)=>{
    document.querySelector('#mjesecBasicDate').innerHTML = months[monthPicked-1]+ ' ' + yyyy +'.';
    let table = document.querySelector('#monthTableBasic'); 
    let sum = 0;
    table.innerHTML = "<tr>"+
                          "<th>Datum</th>"+
                          "<th>Ukupno skenirao gajbi</th>"+
                          "<th>Prva aktivnost</th>"+
                          "<th>Zadnja aktivnost</th>"+
                          "<th>Ukupno sati</th>"+
                          "<th>Novo stanje</th>"+
                          "</tr>";
                          res.forEach(element => {
                            if(element.count){
                              sum+=element.count;
                              let d = element.barcodes[0].timestamp.split(" ")[0];
                              table.innerHTML += "<tr>"+
                                "<td>"+ d +"</td>"+
                                "<td>"+ element.count +"</td>"+
                                "<td>"+ element.barcodes[0].timestamp.split(" ")[1] +"</td>"+
                                "<td>"+ element.barcodes[element.count-1].timestamp.split(" ")[1] +"</td>"+
                                "<td>"+ element.hours +"</td>"+
                                "<td>"+ sum +"</td>"+
                                "</tr>";
                              }
                            });
                            printJS('treciPrint', 'html');
                          });
                        });
/*MONTH BASIC*/

/*MONTH DETAILED*/
$('#drugiPrintBtn').click(()=>{
  let date = yyyy+'-' + monthPicked.toString().padStart(2,'0') +'-01';
  $.get("{{url(session('company_id') . '/api/manager/PDF/month').'/'}}" + document.querySelector('#worker_id').value + '/' +date,(res)=>{
    console.log(res);
    document.querySelector('#mjesecDetaildate').innerHTML = months[monthPicked-1]+ ' ' + yyyy +'.';
    let table = document.querySelector('#drugiPrintContent'); 
    let sum = 0;
    $('#drugiPrintContent').html(' ');
    
    res.forEach(element => {
      if(element.count){
        table.innerHTML += '<div style="margin-left: 80px; margin-top: 70px"  class="printContAl">'
          + '<strong>Datum: </strong> ' + element.barcodes[0].timestamp.split(" ")[0] + '<br>'
          + GenerateHTMLForDay(element,1)
          + '</div>';
        }
      });
      printJS('drugiPrint', 'html');
    });
  });
  /*MONTH DETAILED*/
  
  /*YEAR*/
  $('#cetvrtiPrintBtn').click(()=>{
    $.get("{{url(session('company_id') . '/api/manager/PDF/year').'/'}}" + document.querySelector('#worker_id').value+'/'+yearPicked ,(res)=>{
      let table = document.querySelector('#cetvrtiPrintContent');
      let sumCrates = sumDays = sumHours = 0;
      table.innerHTML = '';
      table.innerHTML += '<table border="1" id="c4table" style="text-align: center;">'+
        '<tr>'+
          '<th>Mjesec</th>'+
          '<th>Ukupno skenirao gajbi</th>'+
          '<th>Ukupno radnih dana</th>'+
          '<th>Ukupno radnih sati</th>'+
          '</tr>';
          let c4table = document.querySelector('#c4table');
          for(let i = 0; i<12;i++){
            sumCrates += res.crates[i];
            sumDays += res.presence.days[i];
            sumHours += res.presence.hours[i];
            if(res.crates[i] || res.presence.days[i] || res.presence.hours[i]){
              c4table.innerHTML += '<tr>'+
                '<td>'+ months[i]+'</td>'+
                '<td>'+res.crates[i]+'</td>'+
                '<td>'+ res.presence.days[i]+ '</td>'+
                '<td>'+ res.presence.hours[i]+ '</td>'+
                '</tr>';
              }
            }
            table.innerHTML+='</table><br><br>';
            table.innerHTML += '<strong>Suma gajbi: </strong> '+ sumCrates +'<br>';
            table.innerHTML +='<strong>Suma radnih dana:</strong>'+ sumDays +'<br>';
            table.innerHTML +='<strong>Suma radnih sati:</strong>'+ sumHours +'<br>';
            
            
            printJS('cetvrtiPrint', 'html');
 });
});
/*YEAR*/

/*PDF*/
</script>
