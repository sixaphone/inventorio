
## About

This project is a inventory mangmant system with logging and order modification ;)

Includes features like :adding and editing material, products, orders with automoatic stock tracking.

## Requirments
    - [PHP version 7.2]
    - [Composer - php dependency manager]
    - [A LAMP stack (on windows best option is xampp or wamp, on linux it is best to install a LAMP server manually)]
    - [Some php libraries documented by Laravel 5.6 version]



## How to test?

Testing is not a difficult proces. The steps to test are:

    - [Create a database and a user]
    - [Copy the .env-example as .env and insert DH host name and pass]
    - [Run command composer install to install dependencies]
    - [Run command php artisan key:generate]
    - [Run command composer dump-autoload]
    - [Run command php artisan migrate --seed to set up DB and fill it with data]
    - [Finally run php artisan serve to start local dev server and thes the app for yourself]



