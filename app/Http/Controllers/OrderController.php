<?php

namespace App\Http\Controllers;

use App\Material;
use App\Order;
use App\Product;
use DB;
use Illuminate\Http\Request;
use Exception;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::selectRaw('orders.id,orders.order_number,orders.created_at,COUNT(order_id) as count,SUM(orders_products_materials.price * orders_products_materials.quantity) as sum')
            ->leftjoin('orders_products_materials', 'order_id', 'orders.id')
            ->latest()
            ->groupBy(['orders.id', 'orders.order_number', 'orders.created_at'])
            ->get();
        return view('pages.orders', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $materials = Material::all();
        return view('pages.neworder', ['products' => $products, 'materials' => $materials]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        DB::beginTransaction();
        try {
            if (Order::where('order_number', $data['ordernumber'])->first()) {
                throw new Exception("Nalog vec postoji");
            }
            $order = [
                'order_number' => $data['ordernumber'],
                'created_at' => $data['date'],
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $orderId = DB::table('orders')->insertGetId($order, 'id');

            for ($i = 1; $i <= $data['count']; $i++) {
                $pid = explode('-', $data['product' . $i]);
                $p = Product::find($pid[0]);

                $mid = explode('-', $data['material' . $i]);
                $m = Material::find($mid[0]);
                if ($p && $m) {
                    DB::table('orders_products_materials')->insert([
                        'order_id' => $orderId,
                        'product_id' => $p->id,
                        'material_id' => $m->id,
                        'quantity' => $data['quantity' . $i],
                        'price' => $data['price' . $i],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    $oldStock = $m->stock;
                    $m->stock -= $data['quantity' . $i];
                    $m->save();
                    DB::table('audits')->insert([
                        'details' => 'Dodana nova stavka za artiakl ' . $p->product_name . ' iskoristen materijal ' . $m->material_name . ', kolicina: ' . $data['quantity' . $i] . ' za nalog pod brojem ' . Order::find($orderId)->order_number,
                        'old_quantity' => $oldStock,
                        'new_quantity' => $m->stock,
                        'product' => $p->id,
                        'material' => $m->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            session()->put('error', 'doslo je do greske');
            return redirect()->back();
        }
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $order = Order::find($id);
        $records = DB::table('orders_products_materials')
            ->selectRaw('product_name,products.measure_unit as pmu,material_name,materials.measure_unit as mmu,orders_products_materials.quantity,orders_products_materials .price')
            ->join('products', 'product_id', 'products.id')
            ->join('materials', 'material_id', 'materials.id')
            ->where('order_id', $id)
            ->orderBy('product_name')
            ->get();
        return response()->json(['order' => $order, 'records' => $records], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $orders = DB::table('orders_products_materials')->where('order_id', $id)->get();
        $o = Order::findOrFail($id);
        $products = Product::all();
        $materials = Material::all();
        return view('pages.editorder', ['orders' => $orders, 'ord' => $o, 'materials' => $materials, 'products' => $products]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $data = $request->all();
        DB::beginTransaction();
        try {

            $ord = Order::findOrFail($id);
            
            for ($i = 1; $i <= $data['count']; $i++) {
                if (isset($data['product' . $i]) && isset($data['material' . $i]) && isset($data['quantity' . $i]) && isset($data['price' . $i])) {
                    if (!empty($data['product' . $i]) && !empty($data['material' . $i]) && !empty($data['quantity' . $i]) && !empty($data['price' . $i]) && $data['record' . $i] != 'd') {
                        $pid = intval(explode('-', $data['product' . $i])[0]);
                        $mid = intval(explode('-', $data['material' . $i])[0]);
                        $p = Product::find($pid);
                        $m = Material::find($mid);
                        if ($data['record' . $i] == 'od' || $data['record' . $i] == 'o') {
                            $opm = DB::table('orders_products_materials')->where('id', $data['id'.$i])->first();
                            $materialOldStock = DB::table('materials')->where('id', $mid)->first()->stock;
                            $newStock = $materialOldStock + $opm->quantity;
                            DB::table('materials')->where('id', $mid)->increment('stock', $opm->quantity);
                            DB::table('audits')->insert([
                                'details' => 'Uklonjena stara stavka ' . $p->product_name . ' iskoristen materijal ' . $m->material_name . ', kolicina materijala : ' . $data['quantity' . $i] . ' za nalog pod brojem ' . $ord->order_number,
                                'old_quantity' => $materialOldStock,
                                'new_quantity' => $newStock,
                                'product' => $pid,
                                'material' => $mid,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]);
                            DB::table('orders_products_materials')->where('id', $data['id'.$i])->delete();

                        }
                        if ($data['record' . $i] != 'od') {

                            $p = Product::find($pid);
                            $m = Material::find($mid);
                            if ($p && $m) {
                                DB::table('orders_products_materials')->insert([
                                    'order_id' => $id,
                                    'product_id' => $p->id,
                                    'material_id' => $m->id,
                                    'quantity' => $data['quantity' . $i],
                                    'price' => $data['price' . $i],
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ]);
                                $oldStock = $m->stock;
                                $m->stock -= $data['quantity' . $i];
                                $m->save();
                                DB::table('audits')->insert([
                                    'details' => 'Dodana nova stavka za artiakl ' . $p->product_name . ' iskoristen materijal ' . $m->material_name . ', kolicina: ' . $data['quantity' . $i] . ' za nalog pod brojem ' . $ord->order_number,
                                    'old_quantity' => $oldStock,
                                    'new_quantity' => $m->stock,
                                    'product' => $p->id,
                                    'material' => $m->id,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ]);
                            }
                        }
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            dd($e);
            session()->put('error', 'doslo je do greske');
            return redirect()->back();
        }
        return $this->index();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
