@extends('layouts.master')
@section('title',$p->product_name)    
@section('navbar_title',$p->product_name)
@section('meta')
@stop
@section('produkti','is-active')
@section('content')

@if(session('status'))
<script>
    swal('Uspjeh', "{{session('status')}}", 'success')
</script>
<?php session()->remove('status');?>
@endif

 <style type="text/css">
      .avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 50px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0,0,0,0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all .2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0,0,0,0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

    </style>
 <div class="container">
    <div class="row">
      <div class="col-md-8">
        @if($p->stock <= 2)
        <div class="c-alert c-alert--warning u-mb-medium">
          <span class="c-alert__icon">
           <i class="feather icon-alert-triangle"></i>
          </span>

          <div class="c-alert__content">
            <h4 class="c-alert__title">Upozorenje!</h4>
            <p>Produkta nema još mnogo na stanju!</p>
          </div>
        </div>
        @endif
   
        <nav class="c-tabs"> 
            <div class="c-tabs__list nav nav-tabs" id="myTab" role="tablist">
              <a class="c-tabs__link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                <span class="c-tabs__link-icon">
                  <i class="feather icon-activity"></i>
                </span>Aktivnost
              </a>
              <a class="c-tabs__link u-hidden-down@tablet" id="nav-postavke-tab" data-toggle="tab" href="#nav-postavke" role="tab" aria-controls="nav-contact" aria-selected="false">
                <span class="c-tabs__link-icon">
                  <i class="feather icon-settings"></i>
                </span>Postavke
              </a>
            </div>

            <div class="c-tabs__content tab-content" id="nav-tabContent">
              <div class="c-tabs__pane active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="c-feed">

                  @foreach ($audits as $a)
                      
                  <div class="c-feed__item c-feed__item--info">
                    <div class="c-feed__item-content">
                      <h6 class="u-text-small">{{$a->details}}</h6>
                    <p>Prije na stanju: {{$a->old_quantity}} -> Sada na stanju: {{$a->new_quantity}}</p>
                    </div>
                    <p class="u-text-xsmall">{{$a->created_at}}</p>
                  </div>
                  @endforeach
            </div><!-- // .c-feed -->

              </div>



<div class="c-tabs__pane" id="nav-postavke" role="tabpanel" aria-labelledby="nav-postavke-tab">
  <form action="/produkti/update" method="POST">
    <input name="id"  value="{{$p->id}}" type="hidden" required>
    {{csrf_field()}}
                        <div class="row">
                          <div class="col-md-6">
                            <div class="c-field" style="margin-top: 10px">
                              <label class="c-field__label" for="input5">Naziv</label>
                              <div class="c-input">
                                <input name="name" class="c-input" value="{{$p->product_name}}" id="input5" required>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="c-field" style="margin-top: 10px">
                              <label class="c-field__label" for="input6">Mjerna jedinica</label>
                              <div class="c-input">
                                <input name="unit" class="c-input" value="{{$p->measure_unit}}" id="input6" required>
                              </div>
                            </div>
                          </div>
                        </div>

                 <div class="row">
                          <div class="col-md-6">
                           <div class="c-field" style="margin-top: 10px">
                              <label class="c-field__label" for="input7">Na stanju</label>
                           <input class="c-input" name="stock" type="number" value="{{$p->stock}}" id="input7" placeholder="Unesite adresu...">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="c-field" style="margin-top: 10px">
                              <label class="c-field__label" for="input8">Za napraviti</label>
                              <input class="c-input" name="make" type="number" value="{{$p->to_make}}" id="input8" placeholder="Unesite telefon...">
                            </div>
                          </div>
                          </div>

                            <div class="o-line" style="margin-top: 20px">
                              <button type="submit" class="c-btn c-btn--info">Spremi</button>
                            </div>
                          </form>
</div>
        </nav>
      </div>

      
      <div class="col-md-4">
        <div class="c-card">
          <div class="u-text-center">
            <h5>{{$p->product_name}}</h5>
            <p>PRODUKT</p>
          </div>

          <span class="c-divider u-mv-small"></span>

          <span class="c-text--subtitle">Mjerna jedinica</span>
          <p class="u-mb-small u-text-large">{{$p->measure_unit}}</p>

          <span class="c-text--subtitle">Na stanju</span>
          <p class="u-mb-small u-text-large">{{$p->stock}}</p>

          <span class="c-text--subtitle">Za napraviti</span>
          <p class="u-mb-small u-text-large">{{$p->to_make}}</p>

        </div>

    </div>

    <div class="row">
      <div class="col-12">
        <footer class="c-footer">
          <p>© 2018 Tishelri</p>
          <span class="c-footer__divider">|</span>
          <nav>
          </nav>
        </footer>
      </div>
    </div>
  </div>
  @stop

    @section('script')
        <script>
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var yyyy = today.getFullYear();

          if(dd<10) {
              dd = '0'+dd
          } 

          if(mm<10) {
              mm = '0'+mm
          } 

          today = yyyy + '-' + mm + '-' + dd ;
        </script>


      <script>
        $('.remove-account').click(function(){
          console.log($(this).data('id'))
        });
      </script>
      <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
      <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

    <script type="text/javascript">
      function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
    </script>
@stop