<form action="/materijali/new" method="POST">
  {{csrf_field()}}
<div class="c-modal modal fade" id="noviMaterijal" tabindex="-1" role="dialog" aria-labelledby="noviMaterijal" style="display: none;" aria-hidden="true">
        <div class="c-modal__dialog modal-dialog" role="document">
            <div class="c-modal__content">
                <div class="c-modal__body">
                    <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                        <i class="feather icon-x"></i>
                    </span>

                    <span class="c-icon c-icon--large u-mb-small">
                      <i class="feather icon-settings"></i>
                    </span>
                    <h3 class="u-mb-small">Novi produkt</h3>
                    
                    <div class="row">
                      <div class="col-lg-12 u-mb-xsmall">
        <div class="c-field">
          <label class="c-field__label" for="input1">Unesite naziv materijala:</label>
          <input class="c-input" type="text" id="input1" name="name" placeholder="Medijapan">
        </div>
        <div class="c-field">
            <label class="c-field__label" for="input1">Unesite mjernu jedinicu:</label>
            <input class="c-input" type="text" id="input1" name="unit" placeholder="kom">
          </div>
          <div class="c-field">
            <label class="c-field__label" for="input1">Unesite cijenu (KM):</label>
            <input class="c-input" type="text" id="input1" name="price" placeholder="500.550">
          </div>
          <div class="c-field">
            <label class="c-field__label" for="input1">Unesite kolicinu na stanju:</label>
            <input class="c-input" type="number" value="0" id="stock" name="stock">
          </div>
          <div class="c-field">
            <label class="c-field__label" for="input1">Unesite kolicinu za nabaviti:</label>
            <input class="c-input" type="number" value="0" id="make" name="make">
          </div>
      </div>
                    </div>

                    <div class="o-line" style="margin-top: 20px">
                      <a href="#" class="c-btn c-btn--info c-btn--outline" data-dismiss="modal" aria-label="Close">Odustani</a>
                      <button type="submit" class="c-btn c-btn--info dodajKulturu">Dodaj</button>
                    </div>
                </div>
            </div><!-- // .c-modal__content -->
        </div><!-- // .c-modal__dialog -->
    </div>
</form>