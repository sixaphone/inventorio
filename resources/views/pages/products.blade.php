@extends('layouts.master')

@section('title','Produkti')
@section('meta')
@stop
@section('produkti','is-active')
@section('navbar_title', 'Produkti')
@section('head')
{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous"> --}}
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
  .c-navbar__title{
    font-weight: normal;
  }
  body{
    font-family: Lato,sans-serif !important;
  }
</style>

    <style type="text/css">
      .avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 50px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0,0,0,0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all .2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0,0,0,0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

    </style>
@stop
@section('content')

@if(session('alert'))
<script>
  swal('Uspjeh', "{{session('alert')}}", 'success')
</script>
<?php session()->remove('alert');?>
@endif
    

@include('includes.dodajProduktModal')
                

     <div class="container">
     	<div class="row">
        <div class="col-12">

          <div class="c-table-responsive@wide" id="users">
                          
            <button class="c-btn c-btn--success u-mb-xsmall" data-toggle="modal" data-target="#noviProdukt"><i class="feather icon-user-plus" style="padding-right: 10px; color: white"></i>Dodaj produkt</button>
           <input class="search" placeholder="Pretraga..." style="border-radius: 25px; padding: 7px 14px; background-color: transparent; border: solid 1px rgba(0, 0, 0, 0.2); width: 200px; box-sizing: border-box; color: #2e2e2e; margin-bottom: 5px; outline: none; margin-bottom: 20px; float: right;" /> 

                <table class="c-table datatable">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head" style="text-align: center; font-weight: bold">Naziv</th>
                      <th class="c-table__cell c-table__cell--head" style="text-align: center; padding-right: 30px; font-weight: bold">Mjerna jedinica</th>
                      <th class="c-table__cell c-table__cell--head" style="text-align: center; font-weight: bold">Cijena</th>
                      <th class="c-table__cell c-table__cell--head" style="text-align: center; font-weight: bold">Na stanju</th>
                      <th class="c-table__cell c-table__cell--head" style="text-align: center; font-weight: bold">Za napraviti</th>
                      <th class="c-table__cell c-table__cell--head" style="text-align: center; font-weight: bold">Opcije</th>
                    </tr>
                  </thead>

                  <tbody class="list">
                    @foreach ($products as $p)
                    <tr class="c-table__row">
                      <td class="c-table__cell">
                        <div class="o-media">
                          <div class="o-media__body">
                            <h6 class="naziv">{{$p->product_name}}</h6>
                          </div>
                        </div>
                      </td>
                      <td class="c-table__cell" style="text-align: center; padding-right: 30px">{{$p->measure_unit}}</td>
                      <td class="c-table__cell" style="text-align: center;">{{$p->price}}</td> 
                      <td class="c-table__cell" style="text-align: center;">{{$p->stock}}</td>
                      <td class="c-table__cell" style="text-align: center;">{{$p->to_make}}</td>
                      <td class="c-table__cell" style="text-align: center;">
                        <div class="c-dropdown dropdown">
                        <a href="{{'/produkti/'.$p->id}}" class="c-btn c-btn--info has-icon " id="dropdownMenuTable1" aria-haspopup="true" aria-expanded="false" role="button">
                            Detaljno
                          </a>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
        </div>
      </div>
     </div>
   

    <form style="visibility:hidden;display:none;" method="POST" id="deleteForm" action="">
      {{csrf_field()}}
      <input type="text" name="id" id="">
    </form>  
@stop




@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script type="text/javascript">
var options = {
  valueNames: [ 'naziv']
};

var userList = new List('users', options);


</script>

@stop
