<?php

namespace App\Http\Controllers;

use App\Product;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('pages.products',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $product = new Product;
        $product->product_name = $data['name'];
        $product->measure_unit = $data['unit'];
        $product->price = $data['price'];
        $product->stock = $data['stock'];
        $product->to_make = $data['make'];
        $product->save();
        session()->put('alert','Dodan novi produkt!');
        DB::table('audits')->insert([
            'details' => 'Dodan novi artikal => ' . $data['name'],
            'old_quantity' => 0,
            'new_quantity' => $data['stock'],
            'product' =>  $product->id,
            'material' => 'prazno',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $product = Product::where('id',$id)->first();
        $audits  = DB::table('audits')->where('product',$product->id)->latest()->take(6)->get();
        return view('pages.product',['p'=>$product,'audits'=>$audits]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $product = Product::findOrFail($data['id']);
        $product->product_name = $data['name'];
        $product->measure_unit = $data['unit'];
        $oldStock = $product->stock; 
        $product->stock = $data['stock'];
        $product->to_make = $data['make'];
        $product->save();
        DB::table('audits')->insert([
            'details' => 'Ažurirani podaci za artikal => ' . $data['name'],
            'old_quantity' => $oldStock,
            'new_quantity' => $data['stock'],
            'product' =>  $product->id,
            'material' => 'prazno',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]);
        $audits  = DB::table('audits')->where('product',$product->id)->latest()->take(6)->get();
        session()->put('status','Produkt ažuriran');

        return view('pages.product',['p'=>$product,'audits'=>$audits]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
