          <div class="c-modal modal fade" id="modalObavijest" tabindex="-1" role="dialog" aria-labelledby="modalObavijest" aria-hidden="true" style="display: none;">
              <div class="c-modal__dialog modal-dialog" role="document">
                  <div class="c-modal__content">
                      <div class="c-modal__body">
                          <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                              <i class="feather icon-x"></i>
                          </span>

                          <span class="c-icon c-icon--large u-mb-small">
                            <i class="feather icon-bell"></i>
                          </span>
                          <h3 class="u-mb-small">Pošalji obavijest</h3>

                          {{-- <div style="display: flex; justify-content: center; align-content: center; flex-direction: column;">
                            <div style="display: flex; justify-content: center; align-content: center;">
                            <select multiple="multiple" id="listaRadnika" name="listaRadnika[]">
                            <option value='Alem'>Alem Tatarević</option>
                            <option value='Mirsad'>Mirsad Halilčević</option>
                            <option value='Anes'>Anes Mehagić</option>
                            <option value='Faris'>Faris Seferagić</option>
                            <option value='Goran'>Goran Šimić</option>
                            </select>
                            </div>
                            <div style="display: flex; justify-content: space-between; margin-top: 5px">
                            <a class="c-btn c-btn--small" style="margin-left: 60px; background-color: #99a5bd; opacity: 0.5" id="oznaciSve">Sve</a>
                            <a class="c-btn c-btn--small" style="margin-right: 60px; background-color: #99a5bd; opacity: 0.5" id="ocistiSve">Očisti</a>
                            </div>
                          </div> --}}

                          <div class="c-field" style="margin-top: 20px; margin-bottom: 20px">
                      <label class="c-field__label" for="input1">Unesite sadržaj poruke:</label>
                      <textarea class="c-input"></textarea>
                    </div>
                          <div class="o-line">
                            <a href="#" class="c-btn c-btn--info c-btn--outline" data-dismiss="modal">Odustani</a>
                            <a href="#" class="c-btn c-btn--info">Pošalji</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>


<div class="o-page__sidebar js-page-sidebar">
        <aside class="c-sidebar">
          <div class="c-sidebar__brand">
         <h1>TISHLERI</h1>
          </div>


          <!-- Scrollable -->
          <div class="c-sidebar__body">
       

            <span class="c-sidebar__title">SISTEM ZA UPRAVLJANJE</span>
            <ul class="c-sidebar__list">
              <li>
                <a class="c-sidebar__link @yield('home')" href="/home">
                  <i class="c-sidebar__icon feather icon-home"></i>Kontrolna ploča
                </a>
              </li>
              <li>
                <a class="c-sidebar__link @yield('produkti')" href="/produkti">
                  <i class="c-sidebar__icon feather icon-package"></i>Produkti
                </a>
              </li>
              <li>
                <a class="c-sidebar__link @yield('materijali')" href="/materijali">
                  <i class="c-sidebar__icon feather icon-inbox"></i>Materijali
                </a>
              </li>
              <li>
                <a class="c-sidebar__link @yield('nalozi')" href="/nalozi">
                  <i class="c-sidebar__icon feather icon-clipboard"></i>Nalozi
                </a>
              </li>
              <li>
                  <a class="c-sidebar__link @yield('nalozi')" href="/nalozi/new">
                    <i class="c-sidebar__icon feather icon-edit"></i>Novi nalog
                  </a>
                </li>
                
            </ul>
            <a class="c-sidebar__footer" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
Odjava <i class="c-sidebar__footer-icon feather icon-power"></i>
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
{{csrf_field()}}
</form>
          </div>
 



        </aside>
</div>