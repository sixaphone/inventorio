<!-- Ovo je base template za svaki apge i bitna linija je ono extend doslovno ovaj page includa
     sve head header i footeri ti samo pravis u pages i naglasi da je to ekstenzija za taj fajl
     i ono samo spoji master i taj page fajl u jedan
     tako da samo ovde head header i footer ukljucujes
-->
<!DOCTYPE html>
<html lang="ba">
    @include('common.head')
    <body>
        <div class="o-page">
     	    @include('common.sidebar')
     		<main class="o-page__content">
    			@include('common.header')
        		@yield('content')
                @include('common.footer')
            </main>
        </div>
    </body>
    
    <script src="{{asset('js/neat.js?v=1.0')}}"></script>
    @yield('script')
</html>
