@extends('layouts.master')

@section('title','Nalozi')    
@section('meta')
@stop
@section('nalozi','is-active')
@section('navbar_title', 'Nalozi')
@section('head')
@stop

<style type="text/css">
  .c-navbar__title{
    font-weight: normal;
  }
  body{
    font-family: Lato,sans-serif !important;
  }
</style>

@section('content')


<style type="text/css">
  .c-table__cell{
    text-align: center;
  }
</style>


<div style="display: flex; justify-content: space-around;
                    align-content: center; align-items: center;">

                    <div style="display: none;">
                    <div id="prviPrint">
                      <div style="display: flex;">
                        <div>
                          <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSExMWFhUXFR4VGBgXFhgfGBoeGB0YGBYYFxUaHiggGBslGx0XITEhJSkrLi4wFx8zODMsNygtLisBCgoKDg0OGxAQGzImICUuLS01MC8rNS0yLS0vKy0tLS0tLTItKy0uLS0vNS0tLS0tLy0tLS0tLS8tLS0tLS0tLf/AABEIAL0BCgMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAABgQFBwMBAgj/xABPEAACAAQDBAcDBwgHBwMFAAABAgADBBEFEiEGMUFRBxMUImFxgTJCkTNSYnKCkqEjQ1SisbLB0xUWNIOT0fAXJFNzpNLhY8LxGERVZJT/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAwQFAgEG/8QALxEAAgIBAwIDBwQDAQAAAAAAAAECAxEEITESQRMiUQVhcYGR4fAUMqGxQlLR8f/aAAwDAQACEQMRAD8A3GCCCACCCCACCCCACCCKvHdoaWjTPUzklDhc95vBEF2c+ABgC0gjJcQ6Wp88lMMo2mcOtnaIPsAgfecHwiiqsHxGtv2+vfId8mToljwIACfFW84hsvrr/cyWuic/2o1PGdvMNpiRNq5eYb0Ql3HmksEj1tChVdM8piVo6KoqCNL2yr+qHYeqiKvDdkaGTup1c85vf/Vbuj0Ai+SYQLKoA5BQB8BFOftGP+KLcdBL/JlNN22x+cfydJIkKdxfVh6lx+5EUpj835XERL8JaLp92Wp/Ewy9a/8AoQvYntvTSXaW8x86GzKJb6HzIAPoYhWsvseIL6Ilekqgsyf8kUbH1rfK4rVtfk83/wB0w/sjxujlG9urqW83X+KmGqU7MoYXswBHkdRH00thvNvMxC9Xc3ySfpql6Cj/ALL6XjUVH3pf8uPR0b06+zV1K+TJ/BRDakgncQfI3joKPnD9Td6j9PUKP9TJq/JYpVp/ePb4K6x9pQ4zJH5LF2e24TQT8S/WH9sNwoxxj0Uq/wCjHS1d67nL09HoK8ranaKSBmWmqeZsAfwaX+76RPk9ME2V/bcNnSwN7y2zDx0cKv65i7ElOQj2yDgPhE0dfYv3JEUtJB/tyScI6UcKn2tUiUTwnKZf67dw+jQ4SZqsAykMp3EEEHyIjL8T2dop9+sp5ZJ95Rlf76WP4wursbMpmL4fWzqc3vkLEoT45bAj6ytFmGvqfOxBLR2Ljc3WCMdo+kTFKPSvpRUSxvnSbBvEkAZT6hIfNmNu6CusJM4dYfzTjLM9FPt+akiLcZxksplaUJR2aGaCCCOjkIIIIAIIIIAIIIIAIIIIAIIIIAIhYvi0imlmbUTFlyxvZj8ABvY+A1hV266RZNCeolL19W2iyl90ncZhGo8FHePgNYRaXZmprpgqsUml29yQDZEG+3dNh5DfbUmIrboVrMiSuqU3sWWJdItdXs0nCpJly72NTNAFvqg3C+uZtfZERcP2Cl5zOrJr1U47zMJy+ViSWHmbeAhhSkCKFQBVAsAosAOQA3QFyP8AzGTbrpz2WyNKrSRjvyyTLplUBVsANAAAAPIDdH2E8Yx/bPa+dNmmVKmBZKMCDLa+cizBi3EA8N2nGHbYvFq2pvUTjK6hlIRUvmDK1iTv5HeeWgjyemlGHXIRuUp9KL7FcUkUyZ500IOF958FUasfACFSr6SpQGaXTz5iXt1hAVDw0bX4GxhG27ef22a1QD7ZEvNfIZYPcCnla17cSeMOFbtb2ylampKKcxmS+r9leqQEW0YG2nC+XhFiOmjFJtZz78JELvbbXHyLrZfbiTWOZWRpczKWAJBVgN9mHEciBGc9IkrLiE/k2RviiA/iDDvsJsU1K3aJ5HW5SqopuEB3kni3DTQa6m8S9pdh5dXP65prp3AmVQvuk65j4G27hHkLKqrm48YEoTnWs85O2HYxLTCpU2bMZAKdFZk9sGwS6XBGbNu0jO6WRR1M/wDJ09ZVPYnLPqJS3A4s4GYC5HvcY06jwSVJphTG8yWAR+Uym4JLWNgBpflwEL8/ZXCwbkKvh2hh/wC/SOKr4Qcud3tj8RLOiUlHj8+oibT4esiamWRLp2y3yS57TGGuhZj7J8uUaJsfi7NhjPNcsyLNGZtTZQSt2425+ERpmDYQVCnqBYk6TrMb78zBrt6mJ1HTUSSXp5bp1bhgV66/tjK1iWJFxyj23UKcEsPKfoK9O4ybysfH7GZbPVNU0+VLlT5isxygl2IFwQzFSbGwufSNQ2f2fenmmZMrZ88ZbBHL5bnezAu1/D+MRsM2RpJc1J0rNmQkjvlhqCuoPgTDCy3BGuotpofQjdEep1XU8Q477HdGm6V5+fiJmN7fz5NS8kU0tQr5R1jMCwvZXuCAqnffW0WH9dTKymqpGVG3TZExZss+Nxa3lvivxPAK/MO9LrZI16ucEEy3ITMuh+kGHlC9XYbUSXDUlLWU7e8AS6k8LMgOYfWuInjCmaSWPr+NfRkEpWRb5/P4/lGrUGIyJ8tZso50bcR+II3g+Bikmba0Kz3kPmTI2XORdCbd7UarY3GotpvjyjxSfIw5qiqQJNUE5coBYk5ZZZRuZjlB/hujI6TI81evYhGe8xrEmxN3Nhrc6/GOKNLGblnhHduocVHHJv0pkYBlBIIuCNxB1BB4i0UON7G0dR3uraVMvcTJdgb8yu5vPf4iIcvpIoQwS01UAtn6sZRbcLBi34Q0UGKyZy55MxJi81YG3mN4PgYr9FtL6t0S9ULNuRboNo8XwvScDXUo94361BzzasOPtZhp7QjS9lNsaTEEzSJl2Au0trCYn1lvqPpC48YXzP8A9XhTxvZNJkwVFKxpqlTmV0JCk+IHsk8xz1Bi5Tr1xZ9StbonzA2yCMt2V6SpkqYKLFlEmbuWfoJb8i1tFv8AOHd55d0aipvrGkmmsooNNcnsEEEengQQQQAQQQQARmG3e381ppw7DO/UG6zJw9mVbRgDuzDi25Tpq2gOk3bSb1n9GUBJqHFprg26pSLkBvdaxuW90HTvEWhbM4HJo5WRASx9t9xY/wAFHAfxuYq6nUqpe8s0ad2PPY82X2VlUgLsesntq81tTc6tlvuBO87zxMfW0+00uksurzW9iUntNc2F7bhf48AYtxNH0vjGT4js7iD1U2ce4c5cTmmqqqoPdIYHMoC24aWjNqxdNyskXrM1RSgiTtC9cxVqypSmltr1SOS4HLqk7znzNvKLPZra6lVpVIiTyGYIsyawYlmNgSLkgE20Ggvuinp6DC5SGdU1Zq5p/NyywzHhdvaP1iQPCLLYLZQiaKuoXqwCWlSje4JvlZ762A3A631Nra27FV4b6+3y3K9bs68x7/PYl7c7EZpct6OSoZCQyJlBYNrm1tcg+O5vCI2y2yuJKoRp5pZOcTCqlWmEi3K4UGw0zW5qbxoVXiMqWpeY4RRvLGw+MKbbV1NY5k4XTNNI0ac4tLXdrrYDT5xB+iYhqsusj0RWfezuyFcJdUthwqalEUs7KqjeWIAHqdIVcQ6RKRDklZ577gspdD5E2v8AZvFlhnRI04ibidW89t/VS2Kyx4ZrX+4EjQ8G2epKRctPIlyhxKqMx+s/tMfEmLFehX+bIZ6t/wCKMop52PVXyFCtOh96ebH4NZv1DE2V0bYtO/tOKCXfeslWPpder/YY12CLUaK48IgldOXLMuToTpG+WqqqaeN2lgH4oT+MWEnobwkDWXNbznOP3SBGgwRLgjyIo6I8I/R2/wAef/3x8TOh/CD+ZmDynzf4sYfYIHhmU7oSw+95c2pln6Lof3kv+MRJnRRWStaXFZo5LNViPiGy/qxrEEeOKfKPVJrgxubhW0VNvlyKxR8xgHPj+b/YYiS+kFZbdXW0s6lf6SsRpyBCsfRTG3xwrKOXNUpNRZinerqGU+YOkQT0tUu30Jo6ixdzLp06kxCSUzJOl3BIViCCNxIBBU+cU2HbC0sp3ZgZqkDKs0A5dTfUWvfQajS3jDXjvRDRTCZlKz0k3gZZJS/1CbqPqFYVa6bi2GX7XJFVTj8/LGqjm+gtp84W+kYqT0tsE1XLb0LMNTXJrrjuJPZ2w+uDTJZMkTCuZlupRuIJFsyix53WNaUIq3GUXHdFwL8gD4xDwrHaarQ9XlcW7yMO8PrIeHjuik272YNTKlmQoDyQQsvcrKbd1eCkWFuHDlFec1bNKzZ8MnjF1xbhuiTTbbSFmmRUynpZgP5yxTwOcbh9K1vGGwMCLg8Li24+XOMmwbN1eSZNlPMS6vS1q5SBf81NbvKCLHlHDZnatqeoyKG7K7gCUXLdXewuj8QDfTiPHWJZ6VPPR2I43tY6+5peNYRLqpfVzVzDeD7ynmp4H/RvFPs9tLU4LMWmrM02hY5ZU0AlpXhbfYD3OWq31WGwTl5/tjlXy5M6W0qaoZGFiCD8QeBG8HhEGn1DqfOxJfT4i43H+lqEmIsyWwZGAZWUghgdQQRvBEdYxHZbHZmC1IpZ7l8PmteXMP5onU35D5w3e8PeEbYjAgEag6iNuE1NdSMqcXF4Z9QQQR0chCd0m7XCgpvyfeqZxySF3m/F7cQtxpxJUcYbpswKpZiAACSTuAGpJ9Iw3C5r4piE3EmB6mU3V0yncANzW52OY/Sf6MR22KuLkySqtzkootdi8AFNLMyaxapm9+a5uTcnNlzcddSeJJPKGQMvzj+MR7TOf7I9tM5/sjAnY5y6mbMa1FYTJGZPnH4mFzazZwVmVe0OiDeosVY8yDqT628IurPz/ZHGbe+seRslF5R74alsylwLYympiHXvzB78yxI+qLWXzAv4x97TbRSqQAfKTm0SUvtEnQE8hf1PAGIm1G0BkZZElTMqpvdloBe19AxHnuHG3IGG3o96PVpT2uqPXVj94sdRLJ4Jza2hb0Fhvu6fTyvfiWcf39irfcqfJXyL2z/R1U1zLU4q5VN6UqHLYfTIPc8h3uZG6NXw+glSJaypMtZaKLBUAAHoIkwRrRiorCMyUnJ5YQQQR6eBBBBABBBBABBBBABBCdjG01Y856fDqTrWQ5Zk+eSlOjfNHvTWHHLu0hO2r2w2hw1UnVMqimSmbKTKEwgE7gxLArfWxsRpAGxQRl2x/TTR1JEuqXssw7mZryT/AHlhk+0LeMagrAi43QB7HhF49ggDOtrei6VNbtNC3ZKkagpcS3O85lHsE81011BhawfaiZKndjxNDIqBoHOiTPmm40F+Y7p8DpG1RQ7X7KU2ISTKnrqNUmD20PNT+0HQ8YhtojasMlrtlB7CljOE01TLZJq3upAbKC6X95GIOUjfCXs/sCJNXnnOsySgzSjYgs9+7nXhl38QTbyiXQVtVh1QMPrmJU6SJ9zlZdygk8Nw11U6G4IMODBvnfjGVOVtGYdmaMI13Yn3PgS/pCPoSh84RHgvFIu4fqeYthMqplNJmG6tx0up4MviI4dFG0MyRNbB6tu/LF6d+DoBfIL8l7y+AYe7Ei0LO2+GsyLVyCVqKY9YrLvspzHzt7Q8iOMXdHqOiXS+GU9Vp+uPUuUbnBFHsXtAldRyqlbAstnUe666TF8s17cwQYvI2zIM76bcbaVRCll/K1biSoB1yadZ8bqn95FfglD2aRLkLuRbE23sdXb1Yk+sVm11T2naBU3pRyQbcM5GcnzvMlf4cMHaj4Rk+0LN1D5mnoa/K54DrD4x4WMHaTyEedo8B8IzdjQw/Q+gRzMVW0mLJSyWnNrYWUE+0x3L/E+AMWnaPAfCFejov6UxdZLC9NSDrJo91m07p82svkj84saanxZ47dyC+3woZGXon2PZAcSqxeqnjMgI1lI27Q7mYW8lsOd9LjwCPY30sbIxG87sIIII9PAggggAggggAgirO0NL2oUXXL2goX6u/esLXvyNjex1sCbWEWkAEEEEAEZv094jLl4W0piM86YioOJyMHYgcgBv+kOcPOOYvJpJD1E9sstBcnj4ADixNgBxJj8n7e7WTcSqmnuMqAZZUu9wiDhfiSbknmeQAgBbjbOhHbuRT086TW1WRUZOpEwkgK1wwSwJtfLcXsN+msVGxfQvPq5HX1E3swYXlrkzOQR3XYZhlXw3nwjOv6Hn9o7KJbGdn6sJlOYte1rEAj1tpAH7QkzVZQykFSLggggg7iCN4j7hf2CwJ6KgkUsx87y1OYjcCzFyqneVUkgHjbhuhggAggggBf232VlYjTNImWDe1Le2qNbQjmDuI4gmMw2PxKYDMoKq4qafu775lGgN+NrjXiGU842+Mo6ZsIMh5GLSR35TrLnAe8jaKT6kp9teUQailWwa79iai11zySvSPY9pV6xFmIQVdQ6m+8MLj8I69lbw+MfPdLN3qi+5yB8oMw5COvZW5D4x52Y8h8YYY6kUfRbV9ixOpw4m0qeOukC+gIFyB5pcf3EbHGEbcFqWfRYgNOpngPbeVvnI8iomL9uN0V7i41B1Bj6HTWddab5MLUQULGkYfsijT63EqnfmqmQHwDTCB93IPSG3sjf6MJvRUc1PPYk61LE2H0JZ/jDuqrxJ+BjI1e90jT0zcalg49lPMfGDs30h8Ylqkv8A0I6KJXhFfpJXY0VNayypbzCy2RGc/ZBP8I6dBOHZaF6pvlKmczE21IQlAPv9YftRH29my1w6pykXMvL94hT+Bht6NZITCqIDjTo/q4zn8SY1vZ8EotmdrLHJpDLBBBGgUgggggAiHi2ICRLMwoz2IGVMuY5iALZmA+Jj89YBTYmu0GRp4M0TesnOZoyNJBu2l7FShICWuLjQW00XajpDpptVKw6nImlpl5swHuLkVnCqRo7XAuRoNRv3AMv9cl/Ran/p/wCdGZ7f4njdfeVJk9npt2UTpfWTB/6rBt30Bprrm0MMWKYglPKedMuEQXawudSBu8yI9wCtWsTrKYdYOIDygw8GRnDL6iAFPouwWfQ1c2srJc55hUqvVvKbMXJMxppaYCToLb7kkndGrf1yX9Fqf+n/AJ0Z/jm2FLSTTInsRMABIXK9r7gSjEA+G/URK2b2gk1zMtNd2WxKkojWOgIDsMwvppu05iAHb+uS/otT/wBP/Og/rkv6LU/9P/OiqOE1P/Ab78n+ZCTUdIdAjshdiVYqSqXW4NjZgbMPEaGAIPSjKxTE5wCSMlNLP5NGmywWJFjMmAORm3gcgTzN6rYTYZpNSs6vp2eWneWXLaSwduGfNMXujfbibcLw9bMY7Lr2daRJkzIAXOUKBfQXZ2AuddPAxf8A9EVP/Af78n+ZADNgmNLUFwJUyWUsT1nV65s1rZHb5p32js2C05qBVGSnXhSgm5Rnynhf8PIkcTFbstQzZbTTMllMyoBcob5TMv7LHmN8MMAEEEEAEEEEAEV20OFrVU06nbdNlsnkSDlPmDY+kWMEAYf0aVOejyOSHkzGlkHhuYD0zEfZhs6kfOhR2Pk5a/FJW4LVEj1mTh+wCHMShzjA1UMWyNqiea1ucxTeMe9ljrcD/wCY8M3yiDCJOqXYWOkDD81BP+iA/wB1gW/VvDVsttlL7FS5zd+zysx5nIuY/G8Ue10y9FVD/wDXmfuMYx2Ti0xVCgmwAA9NI1vZ78jXvM7Wp9Sb9DRejJMiVco3ulWwIB5BV/aphz+98TClhKGmxnEqYX/KP2hR4Mes0/xrfZ8Ibesf5pilrI4uZb0zzWjzN5/eMeZ/P7xj76w/NPwjzOPmH4RWJyg21XNQ1A10l5t590hv4Q89GVQHwqiI4SFT1l9w/iphfq5STEeWytZ1KnTgwIMcegjED2SdRPpMppzAj6Lkm/8AiCaPQRq+zpeVxM/XR3TNNgggjRKARR7ZYAa6lelE95GfeyAG44qw0up4i4vu4xeQQBiP/wBPq/8A5A//AMw/mx3oeiFMPdavtTTWRgFXqgo790NzmYn2r+kbPFNtZ/Z/tp+8IAR8UwlauWaZmKrNZULC1wC66i8V/wDsBpf0ud91P8oYaH5WV/zU/eWNAgDHv9gNL+lzvupAOgGl/S533U/yjYYyzpj6RxRoaOma9S699h+ZVhvvwmEbhwGvzbgJ3SPtYKOn/oakqJ07KSs+dMYFwDr2dXFrgag8h3eYGWUFB1h+UlSxzmOB+Aux8wIlbMYUauqlyL2zEkkgnRQWNwCCb2tv4xodb0cSye7KFuJl1LL6BJkp/TveZgBm2N27wzD6daWSU7uruZhBmOQMzk5NeA0JsABwhvwrpLo5xIAewFyyGXM4A/Jynabz9zgYxyt2BppS5pmeWL2Bm1klEvvtm6k62B4cI7YVgmHy2RxPokdSCGep646WsQLylDeYIGmkAfouirJc5BMlOHQ7mU3GhsR4EG4I3gi0d4zfo32nopVM6zq+nDGomMDMqJQcqxBViLra44AAA3HCH+gr5U9BMkzEmobgPLdWU20NmUkb4AkwQQQAQQQQAQQRVbU4sKSkn1J/Ny2YeLWsijxLFR6wBj2yTdZW4nOG56pgD5POP7CIa/jFf0VYPkw9Jjg5pztNJudQe6h9VUN9qG80S+PxMYWqi52yaNbT3KFaRQwac/wi7aiHj8THw1GOcV/DZOr4iltYyiiqdfzLj7wIH7YSsM2OeZJlTAps8tXGnzgD/GG7pYfqqAqL5psxUUcTY9YfwS3rGoYDhKSKaRIIuZUlJZJ3nIoW5+Ea2gg41v4mdrbFKax6Gb9Kkk0mKUWI7pbjs808BbNYt9h2P9zDOZT8h+MWXSJs726gnSAB1ls8q/z01UeGbVD4OYVujbHe1Uahz+Vk2kzA1811FlY34ld/iG5R5ras4meaa3pzEturfkPx/wAoDKf5o+J/yi09RBfyjP6C14vuKkyW+b+P/iE2rnHCsUlVx0pqn8jP5KdO8fKyv5LM5xpF/ERWbRYIlZIeRMPdYaEb1Yaqw8Qf8omol4U8kdsuuOB2VgRcaiPYy7ou2mmSZhweuNp8ruyGO6Yg9lQx32Gq8103qY1GNlPO5nBBBBHoCKbaz+z/AG0/eEXMR66jSamSYCVuDoSNxuNVIO+AEeh+Vlf81P3ljQIq5Oz1OrBgjXUhheZMOo1Ghax1iNtttGuH0cyqZS+SwVRpmZjlUE8Bc6nkDv3QBR9Ke3qYbIypZqqaCJS8F4GY/wBEcBxOm65H5crquZOmPNmsXd2LMx3kneTEnHsZnVc96ie2aY5ueQ5Ko4KBoBD70N9H3bpvap6/7rKbcRpOca5PqDQtz0HOwHTYLYubLyVE68tpslnlad5LPL6ubc7ibP3SNV33DEQ+y8QK92eMjfP16prW1D7k3+y1jvtcC5v9p/7YRw7PL/F59/2D4CK+AKXE6uW82jCTEb/eCe6wOgkzwTodRF1FJj8yVKalmOURRU6s1lAvJngXY7tbCO39ZKL9Lp/8aX/nACt0x/2SV/zx+48JvR3txOwyfmF3kPpNlX9ofOW+gcc+O7yZ+lbFaedSy1kzpcxhOBIR1YgZHFyAd1yPjFX0W9HMzEZomzgyUinvNuMwj3JZI57zw84A089O+G20k1V7burlfC/WRxHT3Qfo9V8JX/fDUejDCCgQ0UuwAFwXDac3DZj8dYiDogwa9+yny6+db9+AGnZ3HJNbTpUyCTLe9sykEFSVYEHiCCOWkWURcMw6VTylkyJay5aCyqosBc3PxJJv4xKgAjJulrEWrKmRg8hj3nE2oI9xRqoPkt3149Xzhx6QNsJeHU5c2ac91ky/nNbeQNci6EnyG8iFPo+2emSVmVdUc1XUnPMLb1BNwngSdSBu0HuxDfb0Rz3JK4dTG6nlKiKigBVUKoG4BRYD4R9xy6nxg6nxjIyy5hHWPCBHx1XjEXFauXTyZk+Y1klqXPpuA8SbAeJEe7vYbCZtBL7bjdFRLqlOe0TuWmVwGHkstf76NjtGZdC+FO61GKTx+Vq3OTwlqTe3gW08pSRp0bFUOiKRRnLLyEY5tlSNhGJLiEsHslW2WoAGiudS1hxOrjx6we8I2OIGO4RKq5EynnLmSYuU8xxDKeDA2IPMCO5RUlhnieHkpJcxWAZWBUgEEbiDqCDyj6t4xmeDVM7DKk4XVt3L3pppvlZSe6ByB5e6113ZYcu0fSHwMYdydUulmpVHxI5TLrL4iI1RUhTaxOnCK7tPiPgYots9oey0xdWHWucktba34uRyUa+dhxjiLc2ox5O3DoXVLgkbZYJLrUBXNLqJesqaLggg3AJGtr63GoOo8ZmwHSEWcUGI/kqte6rNYLO5a7g58NG4cgk7NdIYa0urIQ7hNA7p+uo9nzGnlDRj+z1PXSxnZSbXSYhBIvyO5l8N37YuV22ad9Ni2K86oWrMHuazBGMYPtrXYUVkV4NTTXypUJq6jcAxPteTd7kWjV8Fxunq5Ym081Zic1OoPJl3qfAgGNKMlJZRRlFxeGWEEEEdHIRT7WbPSq+lmUs0kK9rMN6spurDnYgacdRFxBAH54o+gis68LMnSRIzd50LZyv0UK2DEczYePHfMJw6VTyZciSuWXLUIqjgBzPE8STqSSYlwQAr7S4NOecJ8kK95YlshbK3cLspQnQ+2wINtw14RQz5U5FzPTzxpeyyzMPwk57xo0EAZctYSbdnq/WjqR+Jl2ET5dBUtbJTTNeL5EUeJDHNbyUnwjQoIAqNmsLeRKKzCpdnLtkByi9gFF9TZQoubXNzYXsLYCPYIAIII+J05UUszBVAuSSAABvJJ3CAPuFjbjbWnw6Vmmd+aw/JyVIzP4n5qA72Ppc6Qq7T9KmZzTYWnaJx0M235JPFd2fzNl8TuikwHZnJM7XVzTPqm1LNdlQ/RvvI3A2AHACILtRCpb8+hNVROx7cErZnAZ9TUf0niRvNOsmSfZlD3SVO63BeHtHvHR5YgAm/CKTtP0vw/wDMQKbaOmmTOqSols5NrAjhvsb621+EZE7pWtvH2NBUKG2S97d9Ax72/wCiYilB/wARY8yj54/GIeqRN0R/Mkv+kB80wkbRTnxWtl4XIJEpW6yqccAvC/hcAfTZfmmO22OPtIC09OTMqpxyy0UXK5tA1ufIeu4GHno42OXDqazENUTbPPffduCAnUqtz5kseMaOipk/PL5FLVTivLEZ6OlSVLSVLUKiKEVRuAUWUDyEdoII0ygEEEEALu3GyMnEqcyZndcd6VMAuUby4qdxXiORAIyvBcUnU0/+j8RGSaukuafZmDcvf434Nx3GzDXdoodsNk6bEZPVT11GqTB7csnip4jmp0MRXUxtjiRLVdKt5QsimEY3t9MnmrbrpbS1HdlA7ig94EaEnebbrgHdDqtfV4TMWmxAF5B0k1KgkWG4HidN6nvDhmGsMlXKp6qWMwWdKYAjcyHkQecZcU9LPMln3mg5fqY4TMwweR2JFm1tIJsipUAHe6b2HdNrEgBuBso1uLR5J2jWhn/7jNabSsAxlTQQASTmUFhcEfOHPXNaHvbagafRzEQEstnUDUkqQbAcyuYesZ1sbhsipmtTz1YNlLKwJVgVsGVgdDz3XFjFmu2NsHOXzX2ILKpQkoRNfwvEZVXTrNVc0uYCCrAHwZWG42NxC5WbFtKmdow6c9LN5Anqz4W4DwIZfCOs+fMw+SkulpWnSluWyzO8CTckrYs1/AWj5w7pBpnIE3PTk6AzV7h52cXA9bRUh4kW5VcfL+ixJQfls5JtF0m11HZMTo2ZRp18gCx8WW+Q/Ffqw+YDtvh9ZYSKlCx/Nsckz/DexPmLiKGTPV1DK6OrDQggqR5jQiKDFdhqCfcmUEY+9K7vrl9k+oi1Xr1xNFaek7xZr0EYnJ2bxKl/sWJzAvCXO1UfHMo9EETpW1m0MjSZS09SOaEBvwYfuRbjqK5cMrypmuxr0EZUnS7PTSowqpTxTMR+sij8Y6jpvoR8pT1SeaSv5kSqSfBG00ahBGaDpuwvlUfcT+ZHN+nDDtyyqljyCSv4zI9PMGnwRlc3plDfIYdVzDwuAB+oHiLO6Q8am/IYakq/GaxNvQmX+wxw7IrlnShJ8I16K/FscpqZc1RPlyh9NwCfIE3PpGQVIx2pv19eJKn3ZAsR4XQKf1jFcdl8Ppz1lXOMxzqTOmWJ+yNW9bxBLWVLZbv3E8dJY93sveOGLdMEtmMrD6eZUzODFSsseNrZyPMKPGFqqwjEMQIfEqgiXe4p5Vgo3HUDu38TnPiIvcHmSGlK1NlEo3y5VyjQkHu2HEGIWN7UyKY5GJeZwlyxdtd1+C+uvgYpT1ltj6K1j+/sW4aSuC6pvP8ARZYbhMqQmSTLCLxsNT4sTqx8THuJ1cunlNOmtlRfDUk7gBxJhWOM4vNcCVSpJQ63m62HNjcEeWW8WuIzFmU7dcJVVPpwZvVoDlz2YJml5mJ0vod9iQIr+D5k5vPzyT+Lt5Fj5YFWuxF6xM80zJcl2KSKaV8tUHTUsRbIOJtl38rlUbDqiTU9QoJqEYWEvvENZWFvK4vw33hj2dp8SnVDzwerZ1yNPmy/YXTSSjfAAC2/URpODYfKppfVoSSSWd2N3dm9p2biTp8BF2V8aPKsfD8/9ZUVUrfM8/EmYXMmNKQz0RJpXvqrXAPgbfhrbmd8U21u1KUoEqWvWVMzSXLUXN20UsBrv3LvbhxIqsW2neZNFHh6GfUsbXXVEtvN9xtxJ7o4nhDv0f8AR8lGTU1DdfWPq0w3IS+8S76+BY6nwGkcafSub65rC9D2+9Q8sXlnDo22FanJrq0562aLm5B6oHeoI0LkaEjQDujS5bQYII1eDObyEEEEAEEEEAEEEEARcSw6VUS2lTpazJbCzKwuD/kfHeIyXGNga3DnM7DCZ8gnM9M+rD6nz9OIs2g9uNkgjmUVJYaOoycXlGN4FtlTVByMplTgcplvocw3hSRqb300Om6LGdhlO09KnqiJqXsy6FgQVKuPeFid+6Gva7YKixAEzZeWbawnJpMHK53OPBgYQavZ/GcM+SP9IUw93XrVGvu6t90uPoiM+zRNPNT+Rdhq01ixCjtHsdVpPefIDzEZy91a05bm5HM23Ai/lH1i+JhKdkWqrDdbGTV04bfpYzWQZSOYPDSG/Cdv6SackxjTzAbFJosAeIz7h5NY+EX1XLkz5ZSZkmS24HVTxB+Ot4jd84YVq4O1VGWXWzK+iufOFZ1aE9UUZpi+7oLK1twbMVF+RMXOJdKGWYyyKcMgNgzuQWtxCgaA8NYaME2cpaWZMeSSvWAAqWJAsSe6TqL35ncIRsb2Gnyp3XUhSagcTFQkBlIOYLZiAyjzvbhxjpWU22Ny9O5z4dsIJIu6jpBYSJgnU06mnGU3VFlbIzWOWxZQQb2O4jTfFXsVtjXTqqVImTFdGJzFkUMAqltCtuQGoO+Pja3Eq6fTZJlEZS51LMHzm4uRlQaqOZ1HDjEboopQ9TNmE26uVl9Xb/JD8Y66YKmUml9cnmZO2Mcv6YGzGdqauVUTJcmXTTUWwANQizfZUtmVn01JtputDNh1YZkpHmLldkDMoOYKSLkZhofOMK2rnZqupbeOuceiEqPwEPW2mKT6SkpZMhyoaWEZ13rkVAqKfdJ7xuNe7pxjizT7QjHl/wDDqFqzJvhGgNk+aPVf/EfSNyXTwEZFsXgdNW9Y9XOZnVgAjTO8Ra+ck9463HoYq8cVaKqZaOofKtjmR9x4qSujW8edjxjn9LmTgpbr3fc9d2I9XTt8TbqipCDM5CjmzAD4kxU1u1FJKlia06WULFQVYMWItmChbk2uL8rwjbczZtRh9FUzh+UDFW7ttHBIYjhmCKftRz6KpEl5s/rElsyohTOoa2rBrA7j7OseLTRVbnJ8f9wdO6XWoJDGvSRRFrFZoX55UZfMgNmt6RB6TZYktJqRTyXMwGXnmAuBl7yAJmyG4LG5B3QsbV4BN7bPWRIZkLBl6uWcneVWIBAsO8SLQ+Ynhc2swyRTuVSciyz3791kGRrkX1ylt3OJOmqpxmnz7zjNticWuCu2ExWoqKSqVQrz5QLSxlVfaU9WAqgL7at8YQcBxU09Ws106x1Y5ka+cswIO/UPc8o1DYvZfsExpvac7MmQqEsm8EHUkki2/TeYbu3G97pfnl1+McvUVQlJJZT+R74NslHPYQaqlxHE1CS5HY5BN2eaxDsOWTRreFrHTWGvZ7ZJKSV1Uuxubsx3seZtuHIcI44ztzTU9+snIzD3EGZvUA2X7REVdLimM4npRyBSyD/9xNFiRpqhIN/sK31hCNc7Y9MViP59TyVnhvqbyy32hxKmo1zT5qqSLqguZjfVQG9vE2HjC5h+F4jjFiqmjoW99r55i/RHv38LLrvbdDrsv0V0lO3XVBNZUXDF52qgjiEJNz4sWPK0PoEW6dHXXvyyvZqpz2zsUmymylLh8rq6dLE+251mORxdv4CwHAReQQRbKwQQQQAQQQQAQQQQAQQQQAQQQQAQQQQBSY/snRVg/wB4p0drWD2tMHlMWzDyvCDW9Ec6SS2HV8yVvIlTdUv9ZRa3mjHxjWYI8aTWGeptbow+obG6TSooO0KPfkXJPM2TMfiixxpdv6QnJNE2S4NiroTb0W5HqBG7RDxHCaeeuWfJlzRymIrD9YGKs9FVLtj4FmGstj3yZfSY7SzNEqJRPLOob7pN4sFk8Qu/iBv9Rvi2xDoqwmaSezZCf+HMmKPRA2T8Io26FadbmnrKqTfkyEfqqp/GK8vZq7SJ4+0H3iRKjZylcktSyiSSSerUEk7ySBcnxiZUUqOpR0DKdCrLcH0MVdfsDX0wJXGJzAa2aW5/enGFPEsSxGRp29mt/wCjK/iDHEtBb/t/Z3HWw/1GKfsNQsbmSR4B3A+GbT0iRQbJ0UkgpIBINwXLPbkQHJA9Iz1ts8RvbtR/wpP/AGRMoccxCabdsYf3Ur+CiPXpdQ1hz/lni1NCeej+EaTXUUucuSagdbg2bdcbtI8o8OkyvkpSS/qIFv52GsUWF7I11SNcVmL5SiP3ZqxeS+hdWFqjEambztYA+kwvHkfZ9mMOWx7LXQzlRPmqxSRL+UnS0+s6j8CYpa3bqhl3/KFzyRTr5M1l/GHKg6HsKlgZpcyaRxmTXHxEvKv4Q1YVszRU2silkyz85ZahtN13tmPxiWPs6C/c2yKWvm+EY/IxnEqnSiw2aQd0yaCF+LZU/XMW1L0aYnVa11cJSHfKkAm45E2VR6h42CCLVemqh+1FaeosnyxS2c6OMOo7FJAmTBumTu+9xxFxlQ/VAhtggichCCCCACCCCACCCCACCCCAP//Z" width="200px">
                        </div>
                        <div style="margin-left:15px;margin-top:15px";>

                          <p>
                            STOLARSKA RADNJA "TISHLERI" <br>
                            vl. HALILČEVIĆ ZIJAD  <br>
                            ŽIVINICE,  Priluk bb. 75270 ŽIVINICE <br>
                            ID: 4310896890003 <br>
                            PDV: 310896890003 <br>
                            ŽIRO RAČUN: 13211020164143257 <br>
                          </p>
                        </div>
                         <style type="text/css">
                                .nasAl h2, .nasAl h3{
                                  font-family: Arial;
                                }
                              </style>
                        <div style="margin-left: 100px" class="nasAl">
                          <h2 id="orderNumber"></h2>
                          <h3 id="orderDate"></h3>
                          <h4 id="orderCount"></h3>
                          <h4 id="orderSum"></h3>
                          <h4 id="orderSumPDV"></h3>
                        </div>
                    </div>
                       <style type="text/css">
                      .tabAl th, .tabAl td {
                          padding: 15px;
                          text-align: center;
                      }
                      .tabAl{
                        font-family: Arial;
                      }
                       .tabAl table {
                          border-collapse: collapse;
                          font-family: Arial;
                        }
                      
                      

                       .tabAl table,  .tabAl th,  .tabAl td {
                          border: 1px solid gray;
                      }
                    </style>

                      <div id="contentDiv" style="margin-left: 0; margin-top: 70px" class="tabAl">
                          <table border="1">
                            <tr>
                                <th>Naziv produkta </th>
                                <th>JM produkta</th>
                                <th>Naziv materijala</th>
                                <th>JM materijala</th>
                                <th>Količina</th>
                                <th>Potrošeno</th>
                                <th>Ukupno (Bez PDV)</th>
                                <th>Ukupno (Sa PDV)</th>
                            </tr>
                          </table>
                        </div>

                  </div>
                </div>


<!-- Modal -->
<div class="c-modal modal fade" id="modalPrisustvo" tabindex="-1" role="dialog" aria-labelledby="modalPrisustvo" aria-hidden="true" style="display: none;">
    <div class="c-modal__dialog modal-dialog" style="margin-right:41%;" role="document">
        <div class="c-modal__content" style="width: 155%;">
            <div class="c-modal__body">
                <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                    <i class="feather icon-x"></i>
                </span>

                <span class="c-icon c-icon--large u-mb-small">
                  <i class="feather icon-clipboard"></i>
                </span>
                <h3 class="u-mb-small">Detaljno</h3>
                
                <p class="u-mb-medium" id="modalText"></p>

                <div style="margin-bottom: 20px; margin-top: 30px">
                  <table class="c-table" align="center">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head"><strong>Naziv produkta</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>JM produkta</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Naziv materijala</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>JM materijala</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Količina</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Potrošeno</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Ukupno</strong></th>
                    </tr>
                  </thead>

                  <tbody id="modalTable">
                  </tbody>
                </table>
            </div>

                <div class="o-line">
                  <a href="#" class="c-btn c-btn--info c-btn--outline" data-dismiss="modal" aria-label="Close">Odustani</a>
                  <button style="background-color: #fff; border: 0" onclick="printJS('prviPrint', 'html')">
                  	<a href="#" class="c-btn c-btn--info">Isprintaj</a>
              	 </button>
                </div>
            </div>
        </div><!-- // .c-modal__content -->
    </div><!-- // .c-modal__dialog -->
</div><!-- // .c-modal -->

<div class="container">
      <div class="row">
        <div class="col-12">
          <div class="c-table-responsive@wide">
              <a href="/nalozi/new" class="c-btn c-btn--success u-mb-xsmall"><i class="feather icon-user-plus" style="padding-right: 10px; color: white"></i>Novi nalog</a>

                <table class="c-table" align="center">
                  <thead class="c-table__head">
                    <tr class="c-table__row">
                      <th class="c-table__cell c-table__cell--head"><strong>Datum</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Broj naloga</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Broj stavki</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Cijena bez PDV</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Cijena sa PDV</strong></th>
                      <th class="c-table__cell c-table__cell--head"><strong>Opcije</strong></th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($orders as $o)
                        
                    <tr class="c-table__row">
                      <td class="c-table__cell">{{date_create($o->created_at)->format('d.m.Y. H:i:s')}}</td>
                      <td class="c-table__cell">{{$o->order_number}}</td>
                      <td class="c-table__cell">{{$o->count}}</td>
                      <td class="c-table__cell">{{$o->sum}}</td>
                      <td class="c-table__cell">{{$o->sum *1.17 }}</td>
                      <td class="c-table__cell">
                        <div class="c-dropdown dropdown">
                        <a  style="margin-left:20px;" href="#" data-oid="{{$o->id}}" class="c-btn c-btn--info has-icon dropdown-toggle printBtn"  role="button">Izvještaj</a>
                        
                        <a style="margin-left:20px;" href="/nalozi/update/{{$o->id}}" class="c-btn c-btn--info has-icon"  role="button">Dorada</a>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
     </div>

      @stop

      @section('script')
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
    <script>
      $('.printBtn').click((e)=>{
        e.preventDefault();
        let id = $(e.delegateTarget).data('oid');

        $.get('/nalozi/show/' + id,(res)=>{
          let date = res.order.created_at.split('-');
          let time = date[2].split(' ');
          let total = 0;
          let cnt = 0;
          $('#modalText').html('Radni nalog broj <strong>' + res.order.order_number + '</strong> kreiran <strong>' + time[0] + '.' + date[1] + '.' + date[0] + ' ' + time[1] + '</strong>');
          $('#orderNumber').html('#'+res.order.order_number);
          $('#orderDate').html(time[0] + '.' + date[1] + '.' + date[0] + ' ' + time[1]);
          let table = '<table id="innerTable" border="1">'+
                            '<tr>'+
                                '<th>Naziv produkta </th>'+
                                '<th>JM produkta</th>'+
                                '<th>Naziv materijala</th>'+
                                '<th>JM materijala</th>'+
                                '<th>Količina</th>'+
                                '<th>Potrošeno</th>'+
                                '<th>Ukupno (Bez PDV)</th>'+
                                '<th>Ukupno (Sa PDV)</th>'+
                           ' </tr>';
          $('#modalTable').empty();
          res.records.forEach(element => {
            total += element.price*element.quantity;
            cnt++;
            $('#modalTable').append('<tr class="c-table__row">'+
                      '<td class="c-table__cell">'+element.product_name+'</td>'+
                      '<td class="c-table__cell">'+element.pmu+'</td>'+
                      '<td class="c-table__cell">'+element.material_name+'</td>'+
                      '<td class="c-table__cell">'+element.mmu+'</td>'+
                      '<td class="c-table__cell">'+element.quantity+'</td>'+
                      '<td class="c-table__cell">'+element.price+'</td>'+
                      '<td class="c-table__cell">'+element.price*element.quantity+'</td>'+
                    '</tr>')

           table += '<tr>'+
                      '<td>'+element.product_name+'</td>'+
                      '<td>'+element.pmu+'</td>'+
                      '<td>'+element.material_name+'</td>'+
                      '<td>'+element.mmu+'</td>'+
                      '<td>'+element.quantity+'</td>'+
                      '<td>'+element.price+'</td>'+
                      '<td>'+element.price*element.quantity+'</td>'+
                      '<td>'+Math.round(element.price*element.quantity*1.17*100)/100+'</td>'+
                    '</tr>';      
          });
          $('#modalPrisustvo').modal('show');

          table += '</table>';
          $('#contentDiv').html(table);
          $('#orderCount').html('Stavki: '+ cnt);
          $('#orderSum').html('Cijena bez PDV: '+ total);
          $('#orderSumPDV').html('Cijena sa PDV: ' + (Math.round(total*1.17*100)/100));
        });
      });
        
    </script>
      @stop