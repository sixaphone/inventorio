<?php

namespace App\Http\Controllers;

use App\Material;
use DB;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::all();
        return view('pages.materials',['material'=>$materials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $material = new Material;
        $material->material_name = $data['name'];
        $material->measure_unit = $data['unit'];
        $material->price = $data['price'];
        $material->stock = $data['stock'];
        $material->to_make = $data['make'];
        $material->save();
        session()->put('alert','Dodan novi materijal!');
        DB::table('audits')->insert([
            'details' => 'Dodan novi materijal => ' . $data['name'],
            'old_quantity' => 0,
            'new_quantity' => $data['stock'],
            'material' => $material->id,
            'product' => 'prazno',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $material = Material::where('id',$id)->first();
        $audits  = DB::table('audits')->where('material',$material->id)->latest()->take(6)->get();
        return view('pages.material',['m'=>$material,'audits'=>$audits]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material $material)
    {
        $data = $request->all();
        $material = Material::findOrFail($data['id']);
        $material->material_name = $data['name'];
        $material->measure_unit = $data['unit'];
        $oldStock = $material->stock; 
        $material->stock = $data['stock'];
        $material->to_make = $data['make'];
        $material->save();
        DB::table('audits')->insert([
            'details' => 'Ažurirani podaci za materijal => ' . $data['name'],
            'old_quantity' => $oldStock,
            'new_quantity' => $data['stock'],
            'product' =>  'prazno',
            'material' => $data['id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]);
        $audits  = DB::table('audits')->where('material',$material->id)->latest()->take(6)->get();
        session()->put('status','Produkt ažuriran');

        return view('pages.material',['m'=>$material,'audits'=>$audits]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {
        //
    }
}
